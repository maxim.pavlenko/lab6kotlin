package info.fandroid.lesson_view

data class User (
    var name: String = "",
    var description: String = ""
)